import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthGuard implements CanActivate {
    isAccess;
    role;
    constructor(private authService: AuthService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot): any {
        this.role = route.data.role;
        this.authService.checkRole(this.role).subscribe(x => {
            this.isAccess = x;
            if (!this.isAccess) {
                this.router.navigate(['']);
            }
        })
        return true;

    }
} 
