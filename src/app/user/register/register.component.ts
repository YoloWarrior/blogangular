import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
form:FormGroup;
 @Output() Logged: EventEmitter<any> = new EventEmitter<any>();
  constructor(private auth:AuthService) { }

  ngOnInit() {
    this.form = new FormGroup({
      name:new FormControl('',Validators.required),
      surname:new FormControl('',Validators.required),
      username:new FormControl('',Validators.required),
      password:new FormControl(null,[Validators.minLength(5),Validators.required])
    })
  }
  register(){
    this.auth.register({...this.form.value}).subscribe(x=>{
      this.Logged.emit(true);
    })
  }
}
