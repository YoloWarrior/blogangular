import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, MinLengthValidator, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model:any = {};
  form:FormGroup;
  @Output() Logged: EventEmitter<any> = new EventEmitter<any>();
  constructor(private auth:AuthService) { }

  ngOnInit() {
    this.form = new FormGroup({
      username:new FormControl('',Validators.required),
      password:new FormControl(null,[Validators.minLength(5),Validators.required])
    })
  }
login(){
;
 this.auth.login({...this.form.value}).subscribe(x=>{
              this.Logged.emit(true);
 })
 
}
}
