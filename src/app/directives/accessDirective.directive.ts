import { Directive, TemplateRef, ViewContainerRef, Input, OnInit, AfterContentInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Directive({
  selector: '[role]'
})
export class AccessDirectiveDirective {
  private accessRole = [];
  private isRole;

  constructor(private _templateRef: TemplateRef<any>,
    private _viewContainerRef: ViewContainerRef, private auth: AuthService) { }

  @Input('role') set role(value: string[]) {
    this.accessRole = value;

    this.updateView();
  }
  private updateView(): void {

    if (this.accessRole) {
      this.accessRole.forEach(element => {
        this.auth.checkRole(element).subscribe(x => {

          if (!this.isRole) {
            this._viewContainerRef.clear();
          }

          if (x === true) {
            this.isRole = x;
            this._viewContainerRef.createEmbeddedView(this._templateRef);
            return;
          }
        });
      });
    }
  }
}
