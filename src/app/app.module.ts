import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Ng2PaginationModule} from 'ng2-pagination';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { Routes, RouterModule } from '@angular/router';
import { BlogComponent } from './main/blog/blog.component';
import { LoginComponent } from './user/login/login.component';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { RegisterComponent } from './user/register/register.component';
import { AuthService } from './services/auth.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { LoadCategoryService } from './services/loadCategory.service';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { LoadFileService } from './services/loadFile.service';
import { UploadFileComponent } from './main/uploadFile/uploadFile.component';
import { AdminPanelComponent } from './adminPanel/adminPanel.component';
import { FilterPipe } from './pipes/filter.pipe';
import { SingleArticleComponent } from './main/singleArticle/singleArticle.component';
import { EditElementComponent } from './adminPanel/editElement/editElement.component';
import { TokenInterceptor, TokenInceptProvide } from './interceptors/TokenIntercepotr';
import { ErrorInceptProvide } from './interceptors/errorInterceptor';
import { AccessDirectiveDirective } from './directives/accessDirective.directive';
import { AddArticleComponent } from './main/blog/addArticle/addArticle.component';
import { TagsInputComponent } from './main/blog/addArticle/Tags/tagsInput/tagsInput.component';
import { UniqueFilterPipe } from './pipes/uniqueFilter.pipe';
import { AuthGuard } from './guards/routeGuard';


const appRoutes: Routes =[
   { path: '', component: BlogComponent},
   {path:'article/:id',component:SingleArticleComponent},
   {path:'admin',component:AdminPanelComponent,canActivate:[AuthGuard],data:{role:'admin'}}
];

@NgModule({
   declarations: [
      AppComponent,
      MainComponent,
      BlogComponent,
      LoginComponent,
      RegisterComponent,
      SingleArticleComponent,
      FilterPipe,
      UniqueFilterPipe,
      UploadFileComponent,
      EditElementComponent,
      AdminPanelComponent,
      AddArticleComponent,
      TagsInputComponent,
      AccessDirectiveDirective
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule,
      FormsModule,
      Ng2PaginationModule,
      MyDateRangePickerModule,
      ReactiveFormsModule,
      RouterModule.forRoot(appRoutes)
   ],
   providers: [
      AuthService,
      LoadCategoryService,
      LoadFileService,
      BlogComponent,
      FilterPipe,
      TokenInceptProvide,
      ErrorInceptProvide,
      AuthGuard
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
