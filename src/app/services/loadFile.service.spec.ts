/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { LoadFileService } from './loadFile.service';

describe('Service: LoadFile', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoadFileService]
    });
  });

  it('should ...', inject([LoadFileService], (service: LoadFileService) => {
    expect(service).toBeTruthy();
  }));
});
