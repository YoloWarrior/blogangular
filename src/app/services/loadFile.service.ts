import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadFileService {

  constructor(private http: HttpClient) { }

  load(data) {
    const sub = new Subject<any>();
    this.http.post('/api/file', this.setFormData(data),
    ).subscribe(x => {
      sub.next(x);
      sub.complete()
    })
    return sub;
  }
  setFormData(param) {
    let formData = new FormData();
    for (let i = 0; i < Object.keys(param).length; i++) {
      let key = Object.keys(param)[i];
      let data = param[key];
      formData.append(key, data);
    }
    return formData;
  }
}
