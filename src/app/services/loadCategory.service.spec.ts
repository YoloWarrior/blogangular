/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { LoadCategoryService } from './loadCategory.service';

describe('Service: LoadCategory', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoadCategoryService]
    });
  });

  it('should ...', inject([LoadCategoryService], (service: LoadCategoryService) => {
    expect(service).toBeTruthy();
  }));
});
