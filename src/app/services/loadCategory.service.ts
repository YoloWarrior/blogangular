import { Injectable } from '@angular/core';
import { Subject, from } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators'
@Injectable({
  providedIn: 'root'
})
export class LoadCategoryService {
  headers = new Headers();
  sub = new Subject<any>();
  constructor(private http: HttpClient) { }
getSingle(id){
  const sub = new Subject<any>();
    this.http.get('/api/article/getSingle?='+id).subscribe(x => {
      sub.next(x)
      sub.complete();
    })
    return sub;
}
loadCat(){
  const sub = new Subject<any>();
  this.http.get('/api/article').subscribe(x => {
    sub.next(x)
    sub.complete();
  })
  return sub;
}
  load() {
  const sub = new Subject<any>();
    this.http.get('/api/blog').subscribe(x => {
      sub.next(x)
      sub.complete();
    })
    return sub;
  }
  edit(article){
    const sub = new Subject<any>();
    this.http.put('/api/blog',article).subscribe(x => {
      this.sub.next(x)
      this.sub.complete();
    })
    return this.sub;
  }
  post(article){
    const sub = new Subject<any>();
    this.http.post('/api/article',article).subscribe(x => {
      this.sub.next(x)
      this.sub.complete();
    })
    return this.sub;
  }
  delete(articleID){
    const sub = new Subject<any>();
    this.http.delete('/api/blog?articleId='+articleID).subscribe(x => {
      this.sub.next(x)
      this.sub.complete();
    })
    return this.sub;
  }
}
