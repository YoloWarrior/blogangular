import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'uniqueFilter'
})
export class UniqueFilterPipe implements PipeTransform {

  transform(value: any[], args?: any): any {
    if(value!== undefined && value!== null){
      return _.uniqBy(value, 'name');
  }
  return value;
}
}