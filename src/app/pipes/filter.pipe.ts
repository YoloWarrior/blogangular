import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(items:any,arg:any):any {
  let p;
    if(arg.startDate && arg.startDate!=null){
      console.log(arg)
        return  items.forEach(element => {
         element.articles.filter(x=>x.publishDate>=arg.startDate && x.publishDate<=arg.endDate)
       });
}
    if(arg.filt == undefined || arg.filt==""){
      return items;
    }

    switch(arg.name){
      case'Title Name':{
       return items.filter(x=>x.name.indexOf(arg.filt)>-1)
      }
      case'Tag':{
        return items.filter(x=>x.tags.name.indexOf(arg.filt)>-1)
       }
     
    }
  
}

}
