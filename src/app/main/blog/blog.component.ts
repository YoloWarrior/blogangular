import { Component, OnInit, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IMyDrpOptions, IMyDateRangeModel } from 'mydaterangepicker'
import { LoadCategoryService } from 'src/app/services/loadCategory.service';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { LoadFileService } from 'src/app/services/loadFile.service';
import { FilterPipe } from 'src/app/pipes/filter.pipe';
import * as moment from 'moment';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  categories: Array<Category> = [];
  filteredCategories: Array<Category> = [];
  datePicker: IMyDrpOptions;
  form: FormGroup;
  selectedOption;
  createMode
  file;
  isLogged;
  filt;
  editMode;
  imageName = "Add image";
  editBlog: Blog;
  isFilter;
  isDate;
  startDate;
  EndDate;
  filtByDate: { startDate: string, endDate: string };


  constructor(private http: HttpClient, private loadService: LoadCategoryService,
    private formBuilder: FormBuilder, private loadFile: LoadFileService, private filterDate: FilterPipe, private authService: AuthService) {
  }

  ngOnInit() {
    this.isLogged = false;
    this.fetch();
    this.form = this.formBuilder.group({
      text: new FormControl(''),
      date: new FormControl(''),
      image: new FormControl(''),
    })
  }

  checkLogin($event) {
    this.isLogged = $event;
  }

  filterCategories($event) {
    this.filteredCategories = $event;
  }

  setFile(event) {
  }

  goToEditBLog(blog) {
    this.editBlog = blog;
    this.editMode = true
  }

  editArticle() {
    this.loadService.edit(this.editBlog).subscribe(x => {
      this.editMode = !this.editMode;
      this.fetch();
    })
  }

  deleteBlog(blog) {
    this.loadService.delete(blog.articleID).subscribe(x => {
      this.fetch();
    })
  }

  changeFilterDate() {
    this.isDate = false;
    if (this.selectedOption === "Date")
      this.isDate = !this.isDate;
  }

  onDateRangeChanged(event: IMyDateRangeModel) {
    this.startDate = event.beginDate;
    this.EndDate = event.endDate;
    this.filtByDate = {
      startDate: moment(this.startDate).format('YYYY-MM-DD'),
      endDate: moment(this.EndDate).format('YYYY-MM-DD')
    }
    if (this.filtByDate.startDate != '0000-01-01' || this.filtByDate.endDate != '0000-01-01') {
      this.filteredCategories.map(x => {
        x.articles = x.articles.filter(y => {
          return new Date(y.publishDate) >= new Date(this.filtByDate.startDate) &&
            new Date(y.publishDate) <= new Date(this.filtByDate.endDate) == true
        })
      })
    }
  }

  createBlog($event) {
    this.createMode = !this.createMode;
    this.fetch();
  }

  fetch() {
    this.authService.isLogged().subscribe(x=>{
      if(x)
      this.isLogged = true;
    })
    this.loadService.load().subscribe((x: any) => {
      if (x) {
        this.categories = x;
        this.filteredCategories = x;
        this.filteredCategories.forEach(x => {
          x.articles.map(x => x.publishDate = moment(x.publishDate).format('YYYY-MM-DD'))
        })
      }
    })
  }

}
interface Blog {
  articleId: any,
  name: string,
  shortDescription: string,
  publishDate: any,
  text: string
  icon?: { path?: string },
  tag?: Array<string>
}
interface Category {
  name: string,
  articles?: Array<Blog>
}
