import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-tagsInput',
  templateUrl: './tagsInput.component.html',
  styleUrls: ['./tagsInput.component.less']
})
export class TagsInputComponent {


  public tags: any[];
 @Output() tagsChangeEvents: EventEmitter<any>;

  constructor() {
    this.tags = [];
    this.tagsChangeEvents = new EventEmitter();

  }

  public addTag(tag): void {

    tag = tag.trim();

    if (!tag || this.hasTag(tag)) {

      return;

    }
    this.tags.push(tag);
    this.tagsChangeEvents.emit(tag);

  }

  public hasTag(tag): boolean {

    return (this.tags.indexOf(tag) !== -1);

  }

  public removeTag(tag): void {
    this.tags = this.filterOut(this.tags, tag)
  }
  private filterOut(tags, tag) {

    var filteredTags = tags.filter(
      (currentTag) => {

        return (currentTag !== tag);

      }
    );

    return (filteredTags);

  }
}
