import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LoadCategoryService } from 'src/app/services/loadCategory.service';


@Component({
  selector: 'app-addArticle',
  templateUrl: './addArticle.component.html',
  styleUrls: ['./addArticle.component.css']
})
export class AddArticleComponent implements OnInit {
  Blog: any;
  categories:any[]= [];
  tags:string[] = [];
  selectedCategory:any;
  newArticle:Article = {name:"",shortDescription:"",text:"",category:{name:''},Tags:[]};
  @Output()creationFinished = new EventEmitter();
  constructor(private loadCat: LoadCategoryService) { }
  addTag($event){
   this.tags.push($event);
  }
  setCategory(value){
      this.selectedCategory = value
  }
  Add(){
    this.tags.forEach(element => {
      this.newArticle.Tags.push({name:element})
    });
   this.newArticle.category.name= this.selectedCategory;
   this.loadCat.post(this.newArticle).subscribe(x=>{
     this.creationFinished.emit(true);
   })
  }
  ngOnInit() {
    this.loadCat.loadCat().subscribe((x: any) => {
      console.log(x);
      x.forEach(element => {
          this.categories.push({
            Id: element.categoryID,
            name: element.categoryName
          })
        });
        this.selectedCategory = x[0].categoryName;
    })
  }

}
interface Article{
  name:string,
  shortDescription:string,
  text:string,
  category?:Category;
  Tags?:Array<Tag>;
}
interface Tag{
  name:string
}
interface Category{
  name:string;
}