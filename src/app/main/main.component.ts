import { Component, OnInit, Output } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { LoadCategoryService } from '../services/loadCategory.service';
import { EventEmitter } from '@angular/core';
import { BlogComponent } from './blog/blog.component';
import * as moment from 'moment';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  isReg = false;
  isLogin = false;
  isLogged;
  categories: any;
  filteredCategories;
  isFilter
  name: any;
  @Output() filtered = new EventEmitter();
  @Output() logged = new EventEmitter();

  constructor(private authService: AuthService, private loadService: LoadCategoryService, private blogComp: BlogComponent) { }

  ngOnInit() {
    this.authService.isLogged().subscribe(x => {
      this.name = x.userName
      this.isLogin = false;
      this.isReg = false;
      this.name == undefined ? '' : this.isLogged = true;
    })
    this.loadService.load().subscribe((x: any) => {
      if (x) {
        this.categories = x;
        this.categories.forEach(x => {
          x.articles.map(x => x.publishDate = moment(x.publishDate).format('YYYY-MM-DD'))
        })
      }
    })

  }
  login($event) {
    this.authService.isLogged().subscribe(x => {
      this.isLogged = x.access_token || $event
      this.name = x.userName
      this.isLogin = false;
      this.isReg = false;
      this.logged.emit(true);
    });

  }
  filter(name) {

    this.filteredCategories = this.categories;
    if (name != 'all') {
      this.filteredCategories = this.filteredCategories.filter(x => x.name == name);
      this.isFilter = true;
    }
    if (name == 'all') {
      this.filteredCategories = this.categories;
      this.isFilter = false;
    }
    this.filtered.emit(this.filteredCategories);

  }

  register($event) {
    this.login($event);
  }
  logOut() {
    this.authService.logOut().subscribe(x => {
      this.isLogged = false;
      this.logged.emit(false);
    });

  }
}

