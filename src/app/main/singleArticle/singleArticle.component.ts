import { Component, OnInit } from '@angular/core';
import { LoadCategoryService } from 'src/app/services/loadCategory.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-singleArticle',
  templateUrl: './singleArticle.component.html',
  styleUrls: ['./singleArticle.component.css']
})
export class SingleArticleComponent implements OnInit {
  blog;
  id;

  constructor(private load: LoadCategoryService, private activeRoute: ActivatedRoute) {
    this.activeRoute.params.subscribe(x => {
      this.id = x.id;
      this.fetch()
    })
  }

  ngOnInit() {
  }

  fetch(){
    this.load.getSingle(this.id).subscribe(x => {
      this.blog = x
      console.log(this.blog)
    })
  }

}
