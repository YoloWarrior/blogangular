import { HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HTTP_INTERCEPTORS, HttpInterceptor } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';

export class ErrorInterceptor  implements HttpInterceptor{
	//ошибки будут отображаться в alertify
	intercept(req:HttpRequest<any>,next:HttpHandler):Observable<HttpEvent<any>>{
		return next.handle(req).pipe(
				catchError(error=>{
					if(error instanceof HttpErrorResponse){
						if(error.status ===401){
							 return throwError("Not logged");
                        }
                        if(error.status === 403){
                            return throwError("You dont have access to this")
                        }
						const appError = error.headers.get('Application-Error');
					
						const serverError = error.error;
						let ModalStateError = '';
						if(serverError && typeof serverError ==="object"){
								for(const key in serverError){
									if(serverError[key]){
										ModalStateError +=serverError[key]+'\n';
									}
								}
						}
						return throwError(ModalStateError || serverError|| "OHh, Some Bad Happends");
					}
				})
			);
	}
}

export const ErrorInceptProvide = {
	provide:HTTP_INTERCEPTORS,
	useClass:ErrorInterceptor,
	multi:true
}