import { Component, OnInit } from '@angular/core';
import { LoadCategoryService } from '../services/loadCategory.service';

@Component({
  selector: 'app-adminPanel',
  templateUrl: './adminPanel.component.html',
  styleUrls: ['./adminPanel.component.css']
})
export class AdminPanelComponent implements OnInit {
  byCategory = false;
  categories: any[] = [];
  tags: any[] = [];
  constructor(private loadService: LoadCategoryService) { }

  ngOnInit() {
    this.loadService.load().subscribe((x) => {
      x.forEach(element => {
        this.categories.push({
          id: element.id,
          name: element.name
        })
        element.articles.forEach(art => {
          art.tags.forEach(element => {
            this.tags.push({
              id: element.tagID,
              name: element.name
            })
          });
        });
      });

    })

  }

}
